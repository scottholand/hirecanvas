<!DOCTYPE html>  
<head>
    <meta charset="utf-8">
    <title>HireCanvas | Making the art of entry level recruiting a science</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
	<link rel="shortcut icon" href="http://hirecanvas.com/images/HC_Favicon_16.png">
  <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
  <link rel="apple-touch-icon-precomposed" href="http://hirecanvas.com/images/HC_Favicon_57.png">
  <!-- For first- and second-generation iPad: -->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://hirecanvas.com/images/HC_Favicon_72.png">
  <!-- For iPhone with high-resolution Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://hirecanvas.com/images/HC_Favicon_114.png">
  <!-- For third-generation iPad with high-resolution Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://hirecanvas.com/images/HC_Favicon_144.png">
  <link rel="pingback" href="http://hirecanvas.com/xmlrpc.php">
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,600' rel='stylesheet' type='text/css'>

    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
  </head>
  <body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="navbar-inner" >
        <div class="container" >
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand active" href="home">HireCanvas</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li ><a href="services">Services</a></li>
              <li><a href="about">About</a></li>
              <li><a href="contact">Contact</a></li>
                </ul>
 
        <!-- The drop down menu -->
        <ul class="nav pull-right">
            <form class="form-inline" role="form">
  <div class="form-group">
              <input class="form-control" type="text" placeholder="Email"></div>
           <div class="form-group">
     <input class="form-control" type="password" placeholder="Password"></div>
              <button type="submit" class="btn">Sign in</button>
            </form>
</ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
<legend>Sign Up</legend>
<div class="well">
<form id="signup" class="form-inline" method="post" action="success.php">
<div class="controls">
                <div class="input-prepend">
                    <input type="text" class="form-control input-xlarge" id="fname" name="fname" placeholder="First Name">
            </div>
        </div>
            <div class="controls">
                <div class="form-group">
                    <input type="text" class="input-xlarge" id="lname" name="lname" placeholder="Last Name">
            </div>
        </div>
            <div class="controls">
                <div class="form-group">
                    <input type="text" class="input-xlarge" id="email" name="email" placeholder="Email">
            </div>
        </div>
       
            <div class="controls">
                <div class="input-prepend">
                    <input type="Password" id="passwd" class="input-xlarge" name="passwd" placeholder="Password">
            </div>
        </div>
            <div class="controls">
                <div class="input-prepend">
                    <input type="Password" id="conpasswd" class="input-xlarge" name="conpasswd" placeholder="Re-enter Password">
            </div>
        </div>
          <div class="controls">
           <button type="submit" class="btn btn-success" >Create <span id="IL_AD3" class="IL_AD">My Account</span></button>
    </div>
      </form>
   </div>
</div>


<?php get_footer(); ?>