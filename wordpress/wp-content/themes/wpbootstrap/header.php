<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>HireCanvas | Making the art of entry level recruiting a science</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="http://www.hirecanvas.com/wordpress/wp-content/themes/wpbootstrap/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="http://www.hirecanvas.com/wordpress/wp-content/themes/wpbootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="http://www.hirecanvas.com/wordpress/wp-content/themes/wpbootstrap/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<link href="http://www.hirecanvas.com/wordpress/wp-content/themes/wpbootstrap/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="http://hirecanvas.com/images/HC_Favicon_16.png">
  <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
  <link rel="apple-touch-icon-precomposed" href="http://hirecanvas.com/images/HC_Favicon_57.png">
  <!-- For first- and second-generation iPad: -->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://hirecanvas.com/images/HC_Favicon_72.png">
  <!-- For iPhone with high-resolution Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://hirecanvas.com/images/HC_Favicon_114.png">
  <!-- For third-generation iPad with high-resolution Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://hirecanvas.com/images/HC_Favicon_144.png">
  <link rel="pingback" href="http://hirecanvas.com/xmlrpc.php">
	
 <script src="http://code.jquery.com/jquery.js"></script>

    <script src="http://www.hirecanvas.com/wordpress/wp-content/themes/wpbootstrap/bootstrap/js/bootstrap.js"></script>
    <script src="http://www.hirecanvas.com/wordpress/wp-content/themes/wpbootstrap/bootstrap/js/bootstrap.min.js"></script>

<script src="http://code.jquery.com/jquery-latest.js"></script>

<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="http://github.com/malsup/form/raw/master/jquery.form.js?v2.38"></script>
<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.6/jquery.validate.min.js"></script>


 <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="Home">HireCanvas</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li><a href="services">Services</a></li>
              <li><a href="about">About</a></li>
              <li><a href="contact">Contact</a></li>
              
            </ul>

        <ul class="nav pull-right">

              <li><a href="signup">Register</a></li>
          <li class="divider-vertical"></li>
          <li class="dropdown">
            <a class="dropdown-toggle" href="#" data-toggle="dropdown">Sign In <strong class="caret"></strong></a>
            <div class="dropdown-menu" style="padding: 15px; padding-bottom: 0px;">
       
            <form class="navbar-form pull-right" action="http:///www.hirecanvas.com/rdemo_VD/login.php" method="post">
              <input class="span2" type="text" placeholder="Username" name="username">
              <input class="span2" type="password" placeholder="Password" name="password">
              <button type="submit" class="btn">Sign in</button>
            </form></div></li>
</ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container"  style="padding-top:100px;">