<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>HireCanvas</title>
<link rel="stylesheet" href="http://hirecanvas.com/php/output.css" />
<script src="sorttable.js"></script>
<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
	<link rel="shortcut icon" href="http://hirecanvas.com/images/HC_Favicon_16.png">
  <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
  <link rel="apple-touch-icon-precomposed" href="http://hirecanvas.com/images/HC_Favicon_57.png">
  <!-- For first- and second-generation iPad: -->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://hirecanvas.com/images/HC_Favicon_72.png">
  <!-- For iPhone with high-resolution Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://hirecanvas.com/images/HC_Favicon_114.png">
  <!-- For third-generation iPad with high-resolution Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://hirecanvas.com/images/HC_Favicon_144.png">
  <link rel="pingback" href="http://hirecanvas.com/xmlrpc.php">
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,600' rel='stylesheet' type='text/css'>

</head>
<body>
<div class="menubar">
        <a href="http://hirecanvas.com" target="_blank"><img class="logo" src="http://hirecanvas.com/sdemo_VD/images/HC_52px.jpeg"/></a>
        <div class="nav"> 
    <a href="login.php" style="color:white;">Click here to logout!</a>
        </div>
    </div>
<div id="bg">
  <img src="http://hirecanvas.com/sdemo_VD/images/lehigh.jpeg" alt="">
</div>	
<div id="table">
<?php 
    include "dbConfig.php"; 
session_start(); 
    if (!$_SESSION["valid_user"]) 
    { 
    // User not logged in, redirect to login page 
    Header("Location: login.php"); 
    } 
// create query 
$query = "SELECT * FROM $db.Student"; 

// execute query 
$result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 

// see if any rows were returned 
if (mysql_num_rows($result) > 0) { 
    // yes 
    // print them one after another 
    echo "<table class='sortable'>
<tr>
<th>Unique ID</th>
<th>First Name</th>
<th>Last Name</th>
<th>Email</th>
<th>University</th>
<th>Major</th>
<th>GPA</th>
<th>Graduation Year</th>
<th>Resume</th>
<th>Image</th>
</tr>";

    while($row = mysql_fetch_row($result)) { 
       echo '<tr>';
    foreach($row as $r) {
        echo '<td>'.$r.'</td>';   
    }
    echo '</tr>'; 
    } 
    echo "</table>"; 
} 
else { 
    // no 
    // print status message 
    echo "No rows found!"; 
} 

// free result set memory 
mysql_free_result($result); 

// close connection 
mysql_close($connection); 

?>
</div>
</body>
<html>