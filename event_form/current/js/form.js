$("#myEmail").change(validate).keyup(validate);;

function validate()
{
     var textBox = $(this);
    
    // Obviously just a dummy check
   if (textBox.val().indexOf("@") > 0)
   {
        textBox.addClass("good");        
               textBox.removeClass("error");
   }      
    else   
    {
        textBox.addClass("error");        
               textBox.removeClass("good");
    }   
}

function button() {
document.getElementById('file').click() 

document.getElementById('file').onchange = changebutton;

function changebutton() {
var resume = this.value;
    var lastIndex = resume.lastIndexOf("\\");
    if (lastIndex >= 0) {
        resume = resume.substring(lastIndex + 1);
    }
    document.getElementById('resume').value = resume;
}

}

/*function theFocus(obj) {
    var tooltip = document.getElementById("tooltip");
    tooltip.innerHTML = obj.title;
    tooltip.style.display = "block";
    tooltip.style.top = obj.offsetTop - tooltip.offsetHeight + "px";
    tooltip.style.left = obj.offsetLeft + "px";
}

function theBlur(obj) {
    var tooltip = document.getElementById("tooltip");
    tooltip.style.display = "none";
    tooltip.style.top = "-9999px";
    tooltip.style.left = "-9999px";
}
*/
function validateName(x){
      // Validation rule
      var re = /[A-Za-z -']$/;
      // Check input
      if(re.test(document.getElementById(x).value)){
        // Style green
        document.getElementById(x).style.background ='#ccffcc';
        // Hide error prompt
        document.getElementById(x + 'Error').style.display = "none";
        return true;
      }else{
        // Style red
        document.getElementById(x).style.background ='#e35152';
        // Show error prompt
        document.getElementById(x + 'Error').style.display = "right";
        return false; 
      }
    }
    // Validate email
    function validateEmail(email){ 
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(re.test(document.getElementById(email).value)){
        document.getElementById(email).style.background ='#ccffcc';
        document.getElementById('emailError').style.display = "none";
        return true;
      }else{
        document.getElementById(email).style.background ='#e35152';
                document.getElementById("emailError").style.display = "right";
        return false;
      }
    }
    function validateForm(){
      // Set error catcher
      var error = 0;
      // Check name
      if(!validateName(name)){
        document.getElementById('nameError').style.display = "block";
        error++;
      }
      // Validate email
      if(!validateEmail(document.getElementById('email').value)){
        document.getElementById('emailError').style.display = "block";
        error++;
      }

      // Don't submit form if there are errors
      if(error > 0){
        return false;
      }
    }     