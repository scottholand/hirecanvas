function button() {
document.getElementById('file').click() 

document.getElementById('file').onchange = changebutton;

function changebutton() {
var resume = this.value;
    var lastIndex = resume.lastIndexOf("\\");
    if (lastIndex >= 0) {
        resume = resume.substring(lastIndex + 1);
    }
    document.getElementById('resume').value = resume;
}

}


function validateName(x){
      // Validation rule
      var re = /[A-Za-z -']$/;
      // Check input
      if(re.test(document.getElementById(x).value)){
        // Style green
        document.getElementById(x).style.background ='#ccffcc';
        // Hide error prompt
        document.getElementById(x + 'Error').style.display = "none";
        return true;
      }else{
        // Style red
        document.getElementById(x).style.background ='#e35152';
        // Show error prompt
        document.getElementById(x + 'Error').style.display = "right";
        return false; 
      }
    }
    // Validate email
    function validateEmail(email){ 
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(re.test(email)){
    document.getElementById('email').style.background ='#ccffcc';
    document.getElementById('emailError').style.display = "none";
    return true;
  }else{
    document.getElementById('email').style.background ='#e35152';
    return false;
  }
}
function validateSelect(x){
  if(document.getElementById(x).selectedIndex !== 0){
    document.getElementById(x).style.background ='#ccffcc';
    document.getElementById(x + 'Error').style.display = "none";
    return true;
  }else{
    document.getElementById(x).style.background ='#e35152';
    return false; 
  }
}
 