<?php

 
// include or define your mysql config here
require_once "dbConfig.php";
 
// the columns to be filtered, ordered and returned
// must be in the same order as displayed in the table
$columns = array
(
    "Student.First_Name",
    "Student.Last_Name",
    "Student.Email",
    "Student.University",
    "Student.Major",
    "Student.GPA",
    "Student.Graduation_Year",
    "Student.Resume",
    "Recruiters_Notes.Recruiter_Rating",
    "Recruiters_Notes.Interview",
    "Recruiters_Notes.Division",
    "Recruiters_Notes.Recruiters_Notes"
);
 
// the table being queried
$table = "$db.Student";
 
// any JOIN operations that you need to do
$joins = "LEFT JOIN $db.Recruiters_Notes ON Student.Unique_ID = Recruiters_Notes.Unique_ID";
 
// filtering
$sql_where = "";
if ($_GET['sSearch'] != "")
{
    $sql_where = "WHERE ";
    foreach ($columns as $column)
    {
        $sql_where .= $column . " LIKE '%" . mysql_real_escape_string( $_GET['sSearch'] ) . "%' OR ";
    }
    $sql_where = substr($sql_where, 0, -3);
}
 
// ordering
$sql_order = "";
if ( isset( $_GET['iSortCol_0'] ) )
{
    $sql_order = "ORDER BY  ";
    for ( $i = 0; $i < mysql_real_escape_string( $_GET['iSortingCols'] ); $i++ )
    {
        $sql_order .= $columns[$_GET['iSortCol_' . $i]] . " " . mysql_real_escape_string( $_GET['sSortDir_' . $i] ) . ", ";
    }
    $sql_order = substr_replace( $sql_order, "", -2 );
}
 
// paging
$sql_limit = "";
if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
{
    $sql_limit = "LIMIT " . mysql_real_escape_string( $_GET['iDisplayStart'] ) . ", " . mysql_real_escape_string( $_GET['iDisplayLength'] );
}
 
$main_query = mysql_query("SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $columns) . " FROM {$table} {$joins} {$sql_where} {$sql_order} {$sql_limit}")
    or die(mysql_error());
 
// get the number of filtered rows
$filtered_rows_query = mysql_query("SELECT FOUND_ROWS()")
    or die(mysql_error());
$row = mysql_fetch_array($filtered_rows_query);
$response['iTotalDisplayRecords'] = $row[0];
 
// get the number of rows in total
$total_query = mysql_query("SELECT COUNT(Unique_ID) FROM {$table}")
    or die(mysql_error());
$row = mysql_fetch_array($total_query);
$response['iTotalRecords'] = $row[0];
 
// send back the sEcho number requested
$response['sEcho'] = intval($_GET['sEcho']);
 
// this line is important in case there are no results
$response['aaData'] = array();
 
// finish getting rows from the main query
while ($row = mysql_fetch_row($main_query))
{
     $response['aaData'][] = $row;
}
 
// prevent caching and echo the associative array as json
header('Cache-Control: no-cache');
header('Pragma: no-cache');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($response);
 
?>