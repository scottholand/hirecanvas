<?php
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['username'])) {
header('Location: http://www.hirecanvas.com/wordpress/signup?message=4');
}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>HireCanvas</title>
<link rel="stylesheet" href="http://hirecanvas.com/rdemo_VD/css/form.css" />
<script src="sorttable.js"></script>
<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
	<link rel="shortcut icon" href="http://hirecanvas.com/images/HC_Favicon_16.png">
  <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
  <link rel="apple-touch-icon-precomposed" href="http://hirecanvas.com/images/HC_Favicon_57.png">
  <!-- For first- and second-generation iPad: -->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://hirecanvas.com/images/HC_Favicon_72.png">
  <!-- For iPhone with high-resolution Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://hirecanvas.com/images/HC_Favicon_114.png">
  <!-- For third-generation iPad with high-resolution Retina display: -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://hirecanvas.com/images/HC_Favicon_144.png">
  <link rel="pingback" href="http://hirecanvas.com/xmlrpc.php">
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,600' rel='stylesheet' type='text/css'>

</head>
<body>
<div class="menubar">
        <a href="http://hirecanvas.com" target="_blank"><img class="logo" src="http://hirecanvas.com/sdemo_VD/images/HC_52px.jpeg"/></a>
        <div class="nav"> 
         <ul>
                <li><a href="http://www.hirecanvas.com/rdemo_VD/recruiter.php" >Evaluate</a></li>
                <li><a href="http://www.hirecanvas.com/rdemo_VD/candidate.php" >Candidates</a></li>
                <li><a href="http://www.hirecanvas.com/rdemo_VD/logout.php" >Logout</a><li>

            </ul> 
        </div>
    </div>
<div id="bg">
  <img src="http://hirecanvas.com/sdemo_VD/images/lehigh.jpeg" alt="">
</div>	
<div class="form-container">

<div class="form_title"><a href="http://varidirect.com" target="_blank"><img id="recruiter-logo" src="http://i.imgur.com/gNDSk8k.jpg" width="100%"/></a><h1>&nbsp;&nbsp;Recruiter Candidate Notes Form</h1></div>

<form enctype="multipart/form-data" action="recruiter_notes.php" method="post" class="form" id="myform" onsubmit=" return validateForm();">
<div class="field-container">
<?php
include "dbConfig.php"; 
$query = "SELECT *, CONCAT(First_Name, ' ' ,Last_Name) AS fullname FROM $db.Student";
$result = mysql_query($query) or die ("Error in query: $query. ".mysql_error()); 
if (mysql_num_rows($result) > 0) { 

echo "<div class='styled-select'><select name='fullname' tabindex='1' >";
echo '<option name="fullname" value="selected">Select Candidate</option>';
while ($row = mysql_fetch_array($result)) {

    echo "<option name='" . $row['Unique_ID'] . "' value='" . $row['Unique_ID'] . "'>" . $row['fullname'] . "</option>";
}
echo "</select></div>";
} 
else { 
    // no 
    // print status message 
    echo "No rows found!"; 
} 

// free result set memory 
mysql_free_result($result); 

// close connection 
mysql_close($connection); 

?>

<div class="styled-select">
<select name="rating" id="rating" onblur="validateSelect(name);" required tabindex="3">
<option value="">Rating</option>
<option value="5">5</option>
<option value="4">4</option>
<option value="3">3</option>
<option value="2">2</option>
<option value="1">1</option>
<option value="0">0</option>
</select></div>

<div id="division">
<div name="division" style="width: 40%; float:left; padding-left:10%;">
<div class="recruiter_bucket">Opportunity Fit</div>
<input class="checkbox" name="division[]" type="checkbox" value="Finance">Finance<br>
<input class="checkbox" name="division[]" type="checkbox" value="Operations">Operations<br>
<input  class="checkbox" name="division[]" type="checkbox" value="Technology">Technology<br>
</div>
<div name="interview" style="width: 40%; float:right; ">
<div class="recruiter_bucket">Offer Interview?</div>
<input   class="checkbox" name="interview[]" type="radio" value="Yes">Yes<br>
<input class="checkbox" name="interview[]" type="radio" value="No">No<br>
<input  class="checkbox" name="interview[]" type="radio" value="Maybe">Maybe<br>
</div>
</div>
<textarea class="notes" id="notes" placeholder="Notes" name="notes" required onblur="checkTextField(name);" tabindex="2"></textarea>

<input class="form" type="submit" name="submit" id="submit" value="Save"tabindex="4"/>
<div class="help">Questions? Email us at <a href="mailto:info@hirecanvas.com">info@hirecanvas.com</a></div>
</div>

</form>
</div>
</body>
<html>