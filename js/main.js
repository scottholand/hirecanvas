if (Modernizr.touch){
	MBP.hideUrlBarOnLoad();
	MBP.scaleFix();
	var touchStartOrClick = "touchstart";
	
	
} else {
	var touchStartOrClick = "click";
}

function hasClass(a, b) {
  return a.className.match(RegExp("(\\s|^)" + b + "(\\s|$)"))
}
function addClass(a, b) {
  this.hasClass(a, b) || (a.className += " " + b)
}
function removeClass(a, b) {
  hasClass(a, b) && (a.className = a.className.replace(RegExp("(\\s|^)" + b + "(\\s|$)"), " "))
}




// MAX-Height and such only works on load
$(window).load(function() {
  $(".row .max-height").setAllToMaxHeight();
  //$(".not-mobile .layout-icon-list .row .split-max-height").splitAnd(2,$.fn.setAllToMaxHeight);
  $(".not-mobile .layout-icon-list").setChildMaxHeight(".split-max-height");
	$('.not-mobile [data-spy="scroll"]').each(function () {
    var $spy = $(this).scrollspy('refresh');
  });
	$('.not-mobile #enlarge-poster').sizePoster();

	$('.not-mobile .pad-height').each(function(){ $(this).setWindowHt(); });
});
// DOC Ready
$(document).ready(function(a) {
	var $window = a(window);
	$('.mobile a.tix-btn').removeClass("tix-btn txt-reverse").removeAttr("data-scroll").clone().appendTo('.m-tix-btn');
	$('.tablet a.tix-btn').removeClass("tix-btn txt-reverse").removeAttr("data-scroll").clone().appendTo('.m-tix-btn');
	a("#instagram .carousel").carousel({interval:!1});
	a(".desktop #instagram .item a").hoverClass();
	a("#instagram .item a").preventDefault();
	a("#enlarge-poster").preventDefault();
	a(".layout-carousel .carousel").carousel({interval:!1});
	//$(".carousel .fixed .fill").setAllToMaxHeight();
	a("#The-Experience .carousel").on("slid", function() {
	  var b = a(this).find(".carousel-inner .active").attr("id").replace(/item/i, "caption");
	  a(this).find(".carousel-caption.active").removeClass("active in");
	  a("#" + b).addClass("active").timerTrans()
	});
	var slides = a('.mobile .layout-carousel .carousel')
	slides.on('swipeleft', function() {
	  slides.carousel('next');
	})
	slides.on('swiperight', function() {
	  slides.carousel('prev');
	})
	slides.on('movestart', function(e) {
	  // If the movestart is heading off in an upwards or downwards
	  // direction, prevent it so that the browser scrolls normally.
	  if ((e.distX > e.distY && e.distX < -e.distY) ||
	      (e.distX < e.distY && e.distX > -e.distY)) {
	    e.preventDefault();
	  }
	});
	a("body").attr("data-offset", Math.floor(a("#header").outerHeight(!0) + 5));
	a(".not-mobile #sidenav").scrollspy({offset:0});
	a("#sidenav a").smoothScrollTo();
	a("a.smoothscroll").smoothScrollTo();
	a(window).scroll(function() {
	  curNav = a("#sidenav li.active a").attr("href");
	  a(curNav).hasClass("reverse") ? (a(curNav).addClass("active"), a("#sidenav").addClass("reverse")) : (a(curNav).removeClass("active"), a("#sidenav").removeClass("reverse"))
	});
	a("img.vid-still").videoSwap();

	
	a('[data-scroll="fix-top"]').fixTopOnScroll();
	
	
	
	a('#twitter').sharrre({
	  share: {
	    twitter: true
	  },
	  enableHover: false,
	  enableTracking: true,
	  buttons: { 
	  			url: a("a#twitter").attr("data-url") ,
	  			text: a("a#twitter").attr("data-text") 
	  },
	  click: function(api, options){
	    api.simulateClick();
	    api.openPopup('twitter');
	  }
	});
	a('#facebook').sharrre({
	  share: {
	    facebook: true
	  },
	  enableHover: false,
	  enableTracking: true,
	  buttons: { 
	  			url: a("a#facebook").attr("data-url") ,
	  			text: a("a#facebook").attr("data-text") 
	  },
	  click: function(api, options){
	    api.simulateClick();
	    api.openPopup('facebook');
	  }
	});
	
	//a("a#navToggle").toggleClass();
	a("#nsocial a").preventDefault();
	a("#navToggle").toggleActive();
	a("#sidenav li a").topNavLink();
	if (!Modernizr.backgroundsize){
	a('#page > .bg-cover').bgImgClone();
	}
});
(function(a) {
  a.fn.bgImgClone = function() {
  	return a(this).each(function() {
  	  var id  = a(this).attr("id");
  		var bg_url = document.getElementById(id).style.backgroundImage;	
  		bg_url = bg_url.replace('url(','').replace(')','');
  		a(this).removeAttr("style").prepend('<span class="bg-replaced"><img src="'+bg_url+'" class="bg-cover"></span>');
  	})
  };
	a.fn.preventDefault = function() {
		return a(this).click(function(e) {
			e.preventDefault()
		})
	};
  a.fn.setWindowHeight = function() {
    var b = a(this).height();
    document.body && document.body.offsetWidth && (winW = document.body.offsetWidth, winH = document.body.offsetHeight);
    "CSS1Compat" == document.compatMode && document.documentElement && document.documentElement.offsetWidth && (winW = document.documentElement.offsetWidth, winH = document.documentElement.offsetHeight);
    window.innerWidth && window.innerHeight && (winW = window.innerWidth, winH = window.innerHeight);
    b < winH && a(this).height(winH)
  };
  a.fn.setWindowHt = function() {
    var b = a(this).innerHeight() - a(this).height(), b = a(this).parents("article").innerHeight() - b, b = Math.max(a(this).height(), b);
    document.body && document.body.offsetWidth && (winW = document.body.offsetWidth, winH = document.body.offsetHeight);
    "CSS1Compat" == document.compatMode && document.documentElement && document.documentElement.offsetWidth && (winW = document.documentElement.offsetWidth, winH = document.documentElement.offsetHeight);
    window.innerWidth && window.innerHeight && (winW = window.innerWidth, winH = window.innerHeight);
    b < winH && (totalPad = winH - b, evenPad = (winH - b) / 2, a(this).hasClass("pad-height-bottom") ? a(this).css({"padding-bottom":totalPad}) : a(this).hasClass("pad-height-even") && a(this).css({"padding-top":evenPad, "padding-bottom":evenPad}))
  };
  a.fn.setTopBottomMargins = function() {
    var b = a(this).height();
    document.body && document.body.offsetWidth && (winW = document.body.offsetWidth, winH = document.body.offsetHeight);
    "CSS1Compat" == document.compatMode && document.documentElement && document.documentElement.offsetWidth && (winW = document.documentElement.offsetWidth, winH = document.documentElement.offsetHeight);
    window.innerWidth && window.innerHeight && (winW = window.innerWidth, winH = window.innerHeight);
    var c = Math.round(winH - b), b = Math.round(winH - b);
    a(this).css({"padding-bottom":b, height:c})
  };
  a.fn.setAllToMaxHeight = function() {
    return this.height(Math.max.apply(this, a.map(this, function(b) {
      return a(b).height()
    })))
  };
	a.fn.setChildMaxHeight = function(b) {
    var maxHeight = 0;
		a(b, this).each(function(){
			maxHeight = Math.max(a(this).height(), maxHeight);
		});
		a(b, this).each(function(){
			pad = Math.round((maxHeight-a(this).height())/2);
			a(this).css({"padding-top": pad, "padding-bottom": pad });
		});
  };

  a.fn.setAllToMaxWindowHeight = function() {
    maxHeight = Math.max.apply(this, a.map(this, function(b) {
      return a(b).height()
    }));
    winHeight = a(window).height();
    newHeight = Math.max(maxHeight, winHeight);
    return this.height(newHeight)
  };
  a.fn.carouselEvenHeight = function() {
    maxHeight = Math.max.apply(this, a.map(this, function(b) {
      return a(b).height()
    }));
    imgAttr = a(this).children("img").attr("height");
    imgHeight = a(this).children("img").height();
    ccHeight = a(this).children(".carousel-caption").height();
    newHeight = Math.max(maxHeight, imgHeight + 140);
    return this.height(newHeight)
  };
  a.fn.splitAnd = function(a, c) {
    for(var e = 0, d = a;this.eq(e).length;) {
      c.apply(this.slice(e, d)), e = d, d += a
    }
    return this
  };
	a.fn.sizePoster = function(){
		/*  POSTER SECTION  */
		var posterWidth = a('#enlarge-poster').width();
		a(this).click(function(){
			if(!(a(this).hasClass("enlarged"))){
				a(this).removeClass("small").addClass("enlarged").animate({"width" : (a(this).width()*2)}, 500);
			}else{
				a(this).removeClass("enlarged").addClass("small").animate({"width" : posterWidth}, 500);
			}
		});
	}
  a.fn.timerTrans = function() {
    var b = this;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(function() {
      a(b).addClass("in")
    }, 50)
  };
  a.fn.timerTransOut = function() {
    var b = this;
    this.removeClass("in")

  };
  a.fn.fixTopOnScroll = function() {
    this.before('<span class="row scroll-switch"></span>');
    return this.each(function() {
      var b = this, c = a(this).offset().top;
      a(window).scroll(function() {
        a(this).scrollTop() >= c ? a(b).addClass("affix top").timerTrans() : a(b).removeClass("affix top in")
      })
    })
  };
  a.UpdateScrollspy = function() {
    return a('[data-spy="scroll"]').each(function() {
      a(this).scrollspy("refresh")
    })
  };
  a.fn.hoverClass = function(b) {
    b = b || "hover";
    return a(this).hover(function() {
      a(this).addClass(b).timerTrans();
    }, function() {
      a(this).removeClass(b).timerTransOut();
    })
  };
	a.fn.toggleActive = function() {
	  return a(this).click(function(e) {
	  	var target = a(this).attr("href");
	  	if (a(this).hasClass("active")) {
	  		a(this).removeClass("active");
	  		a(target).removeClass("active");
	    } else {
	    	a(this).addClass("active");
	    	a(target).addClass("active");
	    	
	    }
	    e.preventDefault()
	  })
	}
	a.fn.videoSwap = function() {
	  return a(this).click(function(e) {
	  	var b = this;
	  	var iPane = a(b).parent().find("iframe");
	    a(iPane).attr({
	      src: a(iPane).attr("src") + "&autoplay=1",
	      width: a(b).outerWidth(),
	      height: Math.round(a(b).outerWidth() * 0.6125)
	    });
	     a(b).hide().parent().find("div.vid-holder").show();
	    e.preventDefault()
	  })
	}
	a.fn.topNavLink = function() {
	  return a(this).bind(touchStartOrClick, function() {
	    a("#navToggle").hasClass("active") && (clearTimeout(), setTimeout(function() {
	      a("#sidenav").removeClass("active");
	      a("#navToggle").removeClass("active")
	    }, 900))
	  })
	}
  a.fn.smoothScrollTo = function() {
    return this.live("click", function(b) {
      headerHeight = 0 < a("#header").size ? a("#header").height() : 0;
      a("html,body").animate({scrollTop:a(a(this).attr("href")).offset().top - headerHeight + "px"}, 300);
      b.preventDefault()
    })
  };
  a.fn.radioChange = function(b) {
    return this.each(function() {
      a.browser.msie ? a(this).click(b) : a(this).change(b)
    })
  }
})($);